#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# APPTAG|tcl|create_template|Parses config files and shows all configuration items set
# If running in a UNIX shell, restart under tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require tapp 1.0

package provide create_template 1.0

namespace eval create_template {}

proc create_template::write_default_config { file } {
	file_utils::write_file $file {COPYRIGHT_NOTICE=Copyright (c) %YEAR%, <TODO>, all rights reserved.}
}

cfg set LOG_FILE stdout
cfg file [file rootname [this]].cfg
cfg set HELP \
{Usage: create_template [SCRIPT NAME]

Creates a template script setting up the basic stuff that typically most scripts have.

Warning! May irritate eyes.
}

proc create_template::generate_files { name } {
	set file_name $name.tcl
	set pkg_name  ${name}_pkg.tcl
	set test_name $name.test

	foreach file [list $file_name $pkg_name $test_name] {
		if {[file exists $file]} {
			log STDOUT {Error: file $file already exists, bailing.}
			return
		}
	}

	log STDOUT {Generating file $file_name}
	
	cfg set YEAR [clock format [clock seconds] -format %Y]
	cfg set NAME $name
	
	write_file $file_name
	file attributes $file_name -permissions u+x
	log STDOUT {Generating file $pkg_name}
	write_pkg  $pkg_name
	log STDOUT {Generating file $test_name}
	write_test $test_name

	log STDOUT {Running pkgIndex}
	pkg_mkIndex [file dirname $file_name]

	log STDOUT {Done}
}

proc create_template::write_file { file } {
	file_utils::write_file $file [string map [list \\\\ \\] [cfg subst \
{#!/bin/sh
# %COPYRIGHT_NOTICE%
# If running in a UNIX shell, restart under tclsh on the next line \\
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require %NAME% 1.0

cfg file [file rootname [this]].cfg
cfg set HELP \\
{Usage: %NAME% [OPTION]... [FILE]...

TODO

Optional arguments:
%args%
(arguments are not dependent on which order they come in)

Warning! To %NAME% or not to %NAME%, that's the question. 
}

cfg lappend ARGS \\
	{-h --help}             {display this help}

if {[info exists argv]} {
	%NAME%::init
	%NAME%::main $argv
}

}]]
}

proc create_template::write_pkg { file } {
	file_utils::write_file $file [string map [list \\\\ \\] [cfg subst \
{# %COPYRIGHT_NOTICE%

package require tapp 1.0

package provide %NAME% 1.0

namespace eval %NAME% {}

proc %NAME%::write_default_config { file } {
	file_utils::write_file $file {
#LOG_FILE = %Y-%m-%d_%H.%NAME%.log
LOG_DIR  = .
}
}

proc %NAME%::process_files { files } {

	foreach file $files {
		log INFO {Processing file $file}
	}
}

proc %NAME%::init {} {

	param register -p --option\
		--config OPTION_ENABLED\
		--comment {%NAME% option, TODO}
}

proc %NAME%::main { argv } {
	if {[llength $argv] == 0} {
		lappend argv --help
	}

	param parse $argv remaining_args
	param separate $remaining_args app_args files dirs

	process_files [concat $files $dirs]
}

}]]
}

proc create_template::write_test { file } {
	file_utils::write_file $file [string map [list \\\\ \\] [cfg subst \
{#!/bin/sh
# %COPYRIGHT_NOTICE%
# If running in a UNIX shell, restart under tclsh on the next line \\
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script]

package require tcltest
package require %NAME% 1.0

namespace import tcltest::test

# Simply looking for any exception by calling help.
incr test
test %NAME%.${test} {%NAME% --help} {
	%NAME%::main [list --mute --help]
} {}

}]]
}

proc create_template::main { argv } {

	if {[llength $argv] == 0} {
		lappend argv --help
	}

	param parse $argv remaining_args

	if {[llength $remaining_args] > 1} {
		log STDOUT {Error: Too many parameters: $remaining_args}
		log STDOUT {Expected: create_template <name>}
	} elseif {[llength $remaining_args] == 1} {
		generate_files $remaining_args
	}
}

if {[cfg enabled EXECUTE 1] && [info exists argv]} {
	create_template::main $argv
}
